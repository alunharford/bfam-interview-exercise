package com.example.marketmaker;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AcceptanceTest {
    private final ReferencePriceSource referencePriceSource = mock(ReferencePriceSource.class);
    private final QuoteCalculationEngine engine = mock(QuoteCalculationEngine.class);
    private static class ApplicationNetworkLayer {
        private final TcpServer tcpServer;
        private int port;

        public ApplicationNetworkLayer(QuoteRequestQueue queue) {
            tcpServer = new TcpServer("localhost", 0, new TcpConnectionFactory(queue));
        }

        public void start() {
            tcpServer.start();
            port = tcpServer.getPort();
        }

        public Socket connect() {
            try {
                Socket socket = new Socket("localhost", port);
                tcpServer.applyUpdates();
                return socket;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public void applyUpdates() {
            tcpServer.applyUpdates();
        }
    }

    @Test
    public void responds_to_a_request_with_a_quote() throws IOException {
        when(referencePriceSource.get(123)).thenReturn(55.0);
        when(engine.calculateQuotePrice(123, 55.0, true, 100)).thenReturn(16.0);
        QuoteRequestProcessor quoteRequestProcessor = new QuoteRequestProcessor(engine, referencePriceSource);
        QuoteRequestQueue queue = new QuoteRequestQueue();
        ApplicationNetworkLayer networkLayer = new ApplicationNetworkLayer(queue);
        networkLayer.start();

        Socket socket = networkLayer.connect();
        socket.getOutputStream().write("123 BUY 100\n".getBytes(StandardCharsets.US_ASCII));
        networkLayer.applyUpdates();
        quoteRequestProcessor.process(queue.takeOne());

        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String result = reader.readLine();
        assertEquals("16.0", result);
    }

    @Test
    public void sends_responses_in_the_right_order_when_a_second_request_is_processed_before_the_first_request() throws IOException {
        when(referencePriceSource.get(123)).thenReturn(55.0);
        when(referencePriceSource.get(456)).thenReturn(99.0);
        when(engine.calculateQuotePrice(123, 55.0, true, 100)).thenReturn(16.0);
        when(engine.calculateQuotePrice(456, 99.0, false, 700)).thenReturn(22.0);
        QuoteRequestProcessor quoteRequestProcessor = new QuoteRequestProcessor(engine, referencePriceSource);
        QuoteRequestQueue queue = new QuoteRequestQueue();
        ApplicationNetworkLayer networkLayer = new ApplicationNetworkLayer(queue);
        networkLayer.start();

        Socket socket = networkLayer.connect();
        socket.getOutputStream().write("123 BUY 100\n456 SELL 700\n".getBytes(StandardCharsets.US_ASCII));
        networkLayer.applyUpdates();
        QuoteRequest request1 = queue.takeOne();
        QuoteRequest request2 = queue.takeOne();
        quoteRequestProcessor.process(request2);
        quoteRequestProcessor.process(request1);

        BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        assertEquals("16.0", reader.readLine());
        assertEquals("22.0", reader.readLine());
    }
}
