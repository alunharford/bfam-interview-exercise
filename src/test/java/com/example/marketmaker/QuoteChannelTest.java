package com.example.marketmaker;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketOption;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class QuoteChannelTest {
    private final SocketChannel socketChannel = mock(SocketChannel.class);
    private final QuoteChannel channel = new QuoteChannel(socketChannel);

    @Test
    public void sends_a_price_to_the_socket_channel_for_a_registered_request() throws IOException {
        QuoteRequest request = mock(QuoteRequest.class);

        channel.registerRequest(request);
        channel.sendResponse(request, 14);

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel).write(arg.capture());
        ByteBuffer buffer = arg.getValue();
        assertEquals("14.0\n", convertToString(buffer));
    }

    @Test
    public void sends_a_price_for_a_first_request_before_the_price_for_the_second_request_when_the_second_response_is_known_first() throws IOException {
        QuoteRequest request1 = mock(QuoteRequest.class);
        QuoteRequest request2 = mock(QuoteRequest.class);

        channel.registerRequest(request1);
        channel.registerRequest(request2);
        channel.sendResponse(request2, 16);
        channel.sendResponse(request1, 14);

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel, times(2)).write(arg.capture());
        List<ByteBuffer> buffers = arg.getAllValues();
        assertEquals("14.0\n", convertToString(buffers.get(0)));
        assertEquals("16.0\n", convertToString(buffers.get(1)));
    }

    @Test
    public void sends_a_price_for_a_first_and_second_request_before_the_price_for_the_third_request_when_the_third_and_second_responses_are_known_first() throws IOException {
        QuoteRequest request1 = mock(QuoteRequest.class);
        QuoteRequest request2 = mock(QuoteRequest.class);
        QuoteRequest request3 = mock(QuoteRequest.class);

        channel.registerRequest(request1);
        channel.registerRequest(request2);
        channel.registerRequest(request3);
        channel.sendResponse(request3, 18);
        channel.sendResponse(request2, 16);
        channel.sendResponse(request1, 14);

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel, times(3)).write(arg.capture());
        List<ByteBuffer> buffers = arg.getAllValues();
        assertEquals("14.0\n", convertToString(buffers.get(0)));
        assertEquals("16.0\n", convertToString(buffers.get(1)));
        assertEquals("18.0\n", convertToString(buffers.get(2)));
    }

    @Test
    public void sends_prices_in_the_order_of_requests_when_the_last_result_is_received_first_and_the_first_result_is_received_second() throws IOException {
        QuoteRequest request1 = mock(QuoteRequest.class);
        QuoteRequest request2 = mock(QuoteRequest.class);
        QuoteRequest request3 = mock(QuoteRequest.class);

        channel.registerRequest(request1);
        channel.registerRequest(request2);
        channel.registerRequest(request3);
        channel.sendResponse(request3, 18);
        channel.sendResponse(request1, 14);
        channel.sendResponse(request2, 16);

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel, times(3)).write(arg.capture());
        List<ByteBuffer> buffers = arg.getAllValues();
        assertEquals("14.0\n", convertToString(buffers.get(0)));
        assertEquals("16.0\n", convertToString(buffers.get(1)));
        assertEquals("18.0\n", convertToString(buffers.get(2)));
    }

    @Test
    public void closes_the_channel_if_an_io_exception_occurs_on_write() throws IOException {
        StubClosableChannel.IORunnable closed = mock(StubClosableChannel.IORunnable.class);
        SocketChannel socketChannel = spy(new StubClosableChannel(closed));
        QuoteChannel channel = new QuoteChannel(socketChannel);
        QuoteRequest request = mock(QuoteRequest.class);
        when(socketChannel.write(any(ByteBuffer.class))).thenThrow(new IOException());

        channel.registerRequest(request);
        channel.sendResponse(request, 14);

        verify(closed).run();
    }

    @Test
    public void throws_a_runtime_exception_if_attempting_to_close_the_channel_throws_an_exception() throws IOException {
        SocketChannel socketChannel = spy(new StubClosableChannel(() -> { throw new IOException(); }));
        QuoteChannel channel = new QuoteChannel(socketChannel);
        QuoteRequest request = mock(QuoteRequest.class);
        when(socketChannel.write(any(ByteBuffer.class))).thenThrow(new IOException());

        channel.registerRequest(request);

        assertThrows(RuntimeException.class, () -> channel.sendResponse(request, 14));
    }

    @Test
    public void multiple_threads_can_send_responses_at_the_same_time() throws InterruptedException, IOException {
        ArrayList<QuoteRequest> requests = new ArrayList<>(10000);
        for(int i = 0; i < 10000; i++) {
            QuoteRequest request = new QuoteRequest(channel, i, false, i);
            channel.registerRequest(request);
            requests.add(request);
        }
        CountDownLatch startLatch = new CountDownLatch(10);
        CountDownLatch finishLatch = new CountDownLatch(10);
        for(int i = 0; i < 10; i++) {
            List<QuoteRequest> sublist = requests.subList(i * 1000, i * 1000 + 1000);
            new Thread(() -> {
                startLatch.countDown();
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                }
                for (QuoteRequest request : sublist) {
                    channel.sendResponse(request, request.quantity);
                }
                finishLatch.countDown();
            }).start();
        }
        finishLatch.await();

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel, times(10000)).write(arg.capture());
        List<ByteBuffer> messages = arg.getAllValues();
        for(int i = 0; i < 10000; i++) {
            assertEquals(i + ".0\n", convertToString(messages.get(i)));
        }
    }

    @Test
    public void a_thread_can_add_requests_while_another_is_sending_responses() throws InterruptedException, IOException {
        ArrayList<QuoteRequest> requests = new ArrayList<>(10000);
        for(int i = 0; i < 10000; i++) {
            QuoteRequest request = new QuoteRequest(channel, i, false, i);
            channel.registerRequest(request);
            requests.add(request);
        }
        CountDownLatch startLatch = new CountDownLatch(10);
        CountDownLatch finishLatch = new CountDownLatch(10);
        for(int i = 0; i < 10; i++) {
            List<QuoteRequest> sublist = requests.subList(i * 1000, i * 1000 + 1000);
            new Thread(() -> {
                startLatch.countDown();
                try {
                    startLatch.await();
                } catch (InterruptedException e) {
                }
                for (QuoteRequest request : sublist) {
                    channel.sendResponse(request, request.quantity);
                    channel.registerRequest(new QuoteRequest(channel, 0, false, 0));
                }
                finishLatch.countDown();
            }).start();
        }
        finishLatch.await();

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(socketChannel, times(10000)).write(arg.capture());
        List<ByteBuffer> messages = arg.getAllValues();
        for(int i = 0; i < 10000; i++) {
            assertEquals(i + ".0\n", convertToString(messages.get(i)));
        }
    }

    private String convertToString(ByteBuffer buffer) {
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        return new String(bytes, StandardCharsets.US_ASCII);
    }

    private static class StubClosableChannel extends SocketChannel {
        public static interface IORunnable {
            void run() throws IOException;
        }
        // SocketChannel has many final methods and the actual implementation is package protected

        private final IORunnable actionOnClose;

        public StubClosableChannel(IORunnable actionOnClose) {
            super(mock(SelectorProvider.class));
            this.actionOnClose = actionOnClose;
        }

        @Override
        public SocketChannel bind(SocketAddress local) throws IOException {
            return null;
        }

        @Override
        public <T> SocketChannel setOption(SocketOption<T> name, T value) throws IOException {
            return null;
        }

        @Override
        public <T> T getOption(SocketOption<T> name) throws IOException {
            return null;
        }

        @Override
        public Set<SocketOption<?>> supportedOptions() {
            return null;
        }

        @Override
        public SocketChannel shutdownInput() throws IOException {
            return null;
        }

        @Override
        public SocketChannel shutdownOutput() throws IOException {
            return null;
        }

        @Override
        public Socket socket() {
            return null;
        }

        @Override
        public boolean isConnected() {
            return false;
        }

        @Override
        public boolean isConnectionPending() {
            return false;
        }

        @Override
        public boolean connect(SocketAddress remote) throws IOException {
            return false;
        }

        @Override
        public boolean finishConnect() throws IOException {
            return false;
        }

        @Override
        public SocketAddress getRemoteAddress() throws IOException {
            return null;
        }

        @Override
        public int read(ByteBuffer dst) throws IOException {
            return 0;
        }

        @Override
        public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
            return 0;
        }

        @Override
        public int write(ByteBuffer src) throws IOException {
            return 0;
        }

        @Override
        public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
            return 0;
        }

        @Override
        public SocketAddress getLocalAddress() throws IOException {
            return null;
        }

        @Override
        protected void implCloseSelectableChannel() throws IOException {
            actionOnClose.run();
        }

        @Override
        protected void implConfigureBlocking(boolean block) throws IOException {

        }
    }
}