package com.example.marketmaker;

import org.openjdk.jmh.annotations.*;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 3, time = 5)
@Measurement(iterations = 3, time = 5)
@Fork(1)
public class LatencyTest {
    @State(Scope.Thread)
    public static class Application {
        private AtomicBoolean stop;
        private QuoteRequestQueue queue;
        private OutputStreamWriter socketWriter;
        private BufferedReader socketReader;
        private QuoteRequest poison = new QuoteRequest(null, 0, false, 0);

        @Setup(Level.Iteration)
        public void setup() throws IOException {
            queue = new QuoteRequestQueue();
            TcpServer tcpServer = new TcpServer("localhost", 0, new TcpConnectionFactory(queue));
            tcpServer.start();
            int port = tcpServer.getPort();
            Socket socket = new Socket("localhost", port);
            socketWriter = new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.US_ASCII);
            socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.US_ASCII));
            QuoteRequestProcessor processor = new QuoteRequestProcessor(new QuoteCalculationEngine() {
                @Override
                public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
                    return securityId;
                }
            }, new ReferencePriceSource() {
                @Override
                public void subscribe(ReferencePriceSourceListener listener) {

                }

                @Override
                public double get(int securityId) {
                    return 0;
                }
            });
            stop = new AtomicBoolean(false);
            new Thread(() -> {
                while(!stop.get()) {
                    tcpServer.applyUpdates();
                }
            }).start();
            new Thread(() -> {
                while(!stop.get()) {
                    QuoteRequest request = queue.takeOne();
                    if(request != poison) {
                        processor.process(request);
                    }
                }
            }).start();
        }

        @TearDown
        public void teardown() {
            stop.set(true);
            queue.add(poison);
        }

        private void send(String input) throws IOException {
            socketWriter.write(input);
            socketWriter.flush();
        }

        private String readOutput() throws IOException {
            return socketReader.readLine();
        }
    }

    @Benchmark
    public void single_client_sequential_request_latency(Application application) throws IOException {
        application.send("123 BUY 100\n");
        application.readOutput();
    }
}
