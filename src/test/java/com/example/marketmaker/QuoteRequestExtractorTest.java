package com.example.marketmaker;

import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class QuoteRequestExtractorTest {
    private final QuoteChannel channel = mock(QuoteChannel.class);
    private final QuoteRequestExtractor requestExtractor = new QuoteRequestExtractor(channel);

    @Test
    public void extracts_a_single_well_formed_request() {
        byte[] requestBytes = "123 BUY 100\n".getBytes(StandardCharsets.US_ASCII);
        ByteBuffer buffer = ByteBuffer.wrap(requestBytes);

        List<QuoteRequest> requests = requestExtractor.extractRequests(buffer, requestBytes.length);

        assertEquals(1, requests.size());
        QuoteRequest request = requests.get(0);
        assertEquals(channel, request.channel);
        assertEquals(123, request.securityId);
        assertTrue(request.isBuy);
        assertEquals(100, request.quantity);
    }

    @Test
    public void extracts_a_sell_request() {
        byte[] requestBytes = "881 SELL 9148\n".getBytes(StandardCharsets.US_ASCII);
        ByteBuffer buffer = ByteBuffer.wrap(requestBytes);

        List<QuoteRequest> requests = requestExtractor.extractRequests(buffer, requestBytes.length);

        assertEquals(1, requests.size());
        QuoteRequest request = requests.get(0);
        assertEquals(channel, request.channel);
        assertEquals(881, request.securityId);
        assertFalse(request.isBuy);
        assertEquals(9148, request.quantity);
    }

    @Test
    public void extracts_multiple_requests() {
        byte[] requestBytes = "123 BUY 100\n881 SELL 9148\n".getBytes(StandardCharsets.US_ASCII);
        ByteBuffer buffer = ByteBuffer.wrap(requestBytes);

        List<QuoteRequest> requests = requestExtractor.extractRequests(buffer, requestBytes.length);

        assertEquals(2, requests.size());
        QuoteRequest request1 = requests.get(0);
        assertEquals(channel, request1.channel);
        assertEquals(123, request1.securityId);
        assertTrue(request1.isBuy);
        assertEquals(100, request1.quantity);
        QuoteRequest request2 = requests.get(1);
        assertEquals(channel, request2.channel);
        assertEquals(881, request2.securityId);
        assertFalse(request2.isBuy);
        assertEquals(9148, request2.quantity);
    }

    @Test
    public void extracts_a_request_split_over_two_buffers() {
        requestExtractor.extractRequests(ByteBuffer.wrap("12".getBytes(StandardCharsets.US_ASCII)), 2);
        byte[] requestBytes = "3 BUY 100\n".getBytes(StandardCharsets.US_ASCII);
        List<QuoteRequest> requests = requestExtractor.extractRequests(ByteBuffer.wrap(requestBytes), requestBytes.length);

        assertEquals(1, requests.size());
        QuoteRequest request = requests.get(0);
        assertEquals(channel, request.channel);
        assertEquals(123, request.securityId);
        assertTrue(request.isBuy);
        assertEquals(100, request.quantity);
    }

    @Test
    public void extracts_a_request_split_over_two_buffers_for_the_buy_sell_indicator() {
        requestExtractor.extractRequests(ByteBuffer.wrap("123 BU".getBytes(StandardCharsets.US_ASCII)), 6);
        byte[] requestBytes = "Y 100\n".getBytes(StandardCharsets.US_ASCII);
        List<QuoteRequest> requests = requestExtractor.extractRequests(ByteBuffer.wrap(requestBytes), requestBytes.length);

        assertEquals(1, requests.size());
        QuoteRequest request = requests.get(0);
        assertEquals(channel, request.channel);
        assertEquals(123, request.securityId);
        assertTrue(request.isBuy);
        assertEquals(100, request.quantity);
    }

    @Test
    public void extracts_a_request_split_over_two_buffers_for_the_quantity() {
        requestExtractor.extractRequests(ByteBuffer.wrap("123 BUY 100".getBytes(StandardCharsets.US_ASCII)), 11);
        List<QuoteRequest> requests = requestExtractor.extractRequests(ByteBuffer.wrap("\n".getBytes(StandardCharsets.US_ASCII)), 1);

        assertEquals(1, requests.size());
        QuoteRequest request = requests.get(0);
        assertEquals(channel, request.channel);
        assertEquals(123, request.securityId);
        assertTrue(request.isBuy);
        assertEquals(100, request.quantity);
    }

    @Test
    public void includes_results_only_once() {
        byte[] requestBytes = "123 BUY 100\n".getBytes(StandardCharsets.US_ASCII);
        ByteBuffer buffer = ByteBuffer.wrap(requestBytes);

        requestExtractor.extractRequests(buffer, requestBytes.length);
        List<QuoteRequest> requests = requestExtractor.extractRequests(ByteBuffer.wrap("123 BU".getBytes(StandardCharsets.US_ASCII)), 6);

        assertEquals(Collections.emptyList(), requests);
    }
}