package com.example.marketmaker;

import org.junit.jupiter.api.Test;
import org.mockito.InOrder;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;

class TcpConnectionTest {
    private final QuoteRequestExtractor extractor = mock(QuoteRequestExtractor.class);
    private final QuoteRequestQueue queue = mock(QuoteRequestQueue.class);
    private final TcpConnection connection = new TcpConnection(extractor, queue);

    @Test
    public void adds_an_extracted_request_to_the_queue() {
        ByteBuffer buffer = mock(ByteBuffer.class);
        QuoteRequest request = mock(QuoteRequest.class);
        when(extractor.extractRequests(buffer, 4)).thenReturn(Collections.singletonList(request));

        connection.received(buffer, 4);

        verify(queue).add(request);
    }

    @Test
    public void processes_multiple_requests_within_the_same_buffer() {
        ByteBuffer buffer = mock(ByteBuffer.class);
        QuoteRequest request1 = mock(QuoteRequest.class);
        QuoteRequest request2 = mock(QuoteRequest.class);
        when(extractor.extractRequests(buffer, 4)).thenReturn(Arrays.asList(request1, request2));

        connection.received(buffer, 4);

        verify(queue).add(request1);
        verify(queue).add(request2);
    }

    @Test
    public void registers_the_quote_requests_with_the_quote_channel() {
        ByteBuffer buffer = mock(ByteBuffer.class);
        QuoteRequest request = mock(QuoteRequest.class);
        when(extractor.extractRequests(buffer, 4)).thenReturn(Collections.singletonList(request));

        connection.received(buffer, 4);

        verify(request).register();
    }

    @Test
    public void registers_requests_before_adding_them_to_the_queue() {
        ByteBuffer buffer = mock(ByteBuffer.class);
        QuoteRequest request = mock(QuoteRequest.class);
        when(extractor.extractRequests(buffer, 4)).thenReturn(Collections.singletonList(request));

        connection.received(buffer, 4);

        InOrder order = inOrder(request, queue);
        order.verify(request).register();
        order.verify(queue).add(request);
    }
}