package com.example.marketmaker;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class QuoteRequestProcessorTest {
    private final QuoteCalculationEngine calculationEngine = mock(QuoteCalculationEngine.class);
    private final ReferencePriceSource referencePriceSource = mock(ReferencePriceSource.class);
    private final QuoteRequestProcessor processor = new QuoteRequestProcessor(calculationEngine, referencePriceSource);

    @Test
    public void sends_a_calculated_price_to_the_channel() {
        when(referencePriceSource.get(17)).thenReturn(449.0);
        when(calculationEngine.calculateQuotePrice(17, 449.0, true, 44)).thenReturn(82.0);
        QuoteChannel channel = mock(QuoteChannel.class);
        QuoteRequest request = new QuoteRequest(channel, 17, true, 44);

        processor.process(request);

        verify(channel).sendResponse(request, 82.0);
    }

    @Test
    public void calculates_price_for_a_sell() {
        when(referencePriceSource.get(17)).thenReturn(449.0);
        when(calculationEngine.calculateQuotePrice(17, 449.0, false, 44)).thenReturn(82.0);
        QuoteChannel channel = mock(QuoteChannel.class);
        QuoteRequest request = new QuoteRequest(channel, 17, false, 44);

        processor.process(request);

        verify(channel).sendResponse(request, 82.0);
    }
}