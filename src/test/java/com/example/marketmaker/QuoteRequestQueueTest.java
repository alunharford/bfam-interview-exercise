package com.example.marketmaker;

import org.junit.jupiter.api.Test;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class QuoteRequestQueueTest {
    private final QuoteRequestQueue queue = new QuoteRequestQueue();

    @Test
    public void allows_getting_a_queued_item() {
        QuoteRequest request = mockQuoteRequest();

        queue.add(request);
        QuoteRequest actual = queue.takeOne();

        assertSame(request, actual);
    }

    @Test
    public void supports_multiple_items_on_the_queue() {
        QuoteRequest request1 = mockQuoteRequest();
        QuoteRequest request2 = mockQuoteRequest();

        queue.add(request1);
        queue.add(request2);
        QuoteRequest actual1 = queue.takeOne();
        QuoteRequest actual2 = queue.takeOne();

        assertSame(request1, actual1);
        assertSame(request2, actual2);
    }

    private QuoteRequest mockQuoteRequest() {
        return new QuoteRequest(mock(QuoteChannel.class), 0, false, 0);
    }

    @Test
    public void performance_removing_an_item_is_faster_than_a_naive_list() throws RunnerException {
        Options naive = new OptionsBuilder()
                .include(this.getClass().getName() + ".Performance_removing_an_item_is_faster_than_a_naive_list.naive")
                .shouldFailOnError(true)
                .shouldDoGC(true)
                .build();

        RunResult naiveResults = new Runner(naive).run().iterator().next();

        Options actual = new OptionsBuilder()
                .include(this.getClass().getName() + ".Performance_removing_an_item_is_faster_than_a_naive_list.actual")
                .shouldFailOnError(true)
                .shouldDoGC(true)
                .build();

        RunResult actualResults = new Runner(actual).run().iterator().next();

        double naiveScore = naiveResults.getPrimaryResult().getScore();
        double actualScore = actualResults.getPrimaryResult().getScore();
        assertTrue(actualScore < naiveScore / 2);
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @Warmup(iterations = 2, time = 1)
    @Measurement(iterations = 2, time = 1)
    @Fork(1)
    public static class Performance_removing_an_item_is_faster_than_a_naive_list {
        @Benchmark
        public void naive() {
            QuoteRequest request = new QuoteRequest(null, 0, false, 0);
            ArrayList<QuoteRequest> list = new ArrayList<>();
            for(int i = 0; i < 10000; i++) {
                list.add(request);
            }
            for(int i = 0; i < 10000; i++) {
                list.remove(0);
            }
        }

        @Benchmark
        public void actual() {
            QuoteRequestQueue queue = new QuoteRequestQueue();

            QuoteRequest request = new QuoteRequest(null, 0, false, 0);
            for(int i = 0; i < 10000; i++) {
                queue.add(request);
            }
            for(int i = 0; i < 10000; i++) {
                queue.takeOne();
            }
        }
    }

    @Test
    public void supports_multiple_readers_without_corrupting_state() throws InterruptedException {
        QuoteRequest request = new QuoteRequest(null, 0, false, 0);
        CountDownLatch startingLine = new CountDownLatch(11);
        CountDownLatch finishingLine = new CountDownLatch(11);
        Runnable ready = () -> {
            startingLine.countDown();
            try {
                startingLine.await();
            } catch (InterruptedException e) {
            }
        };
        new Thread(() -> {
            ready.run();
            for(int i = 0; i < 10000000; i++) {
                queue.add(request);
            }
            finishingLine.countDown();
        }).start();
        AtomicInteger resultsRead = new AtomicInteger();
        for(int i = 0; i < 10; i++) {
            new Thread(() -> {
                ready.run();
                for(int j = 0; j < 1000000; j++) {
                    if(queue.takeOne() != null) {
                        resultsRead.incrementAndGet();
                    }
                }
                finishingLine.countDown();
            }).start();
        }
        finishingLine.await();
    }
}