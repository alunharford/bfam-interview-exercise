package com.example.marketmaker;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class QuoteRequestTest {
    private final QuoteChannel channel = mock(QuoteChannel.class);
    private final QuoteRequest request = new QuoteRequest(channel, 17, true, 19);

    @Test
    public void registers_itself_with_the_channel() {
        request.register();

        verify(channel).registerRequest(request);
    }

    @Test
    public void does_not_register_itself_with_the_channel_until_requested() {
        verify(channel, never()).registerRequest(request);
    }
}