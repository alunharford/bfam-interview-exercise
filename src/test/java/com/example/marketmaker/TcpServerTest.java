package com.example.marketmaker;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TcpServerTest {
    private final TcpConnectionFactory connectionFactory = mock(TcpConnectionFactory.class);
    private final TcpServer tcpServer = new TcpServer("localhost", 0, connectionFactory);

    @Test
    public void calls_a_subscriber_when_a_client_connects() throws IOException {
        tcpServer.start();

        Socket socket = new Socket("localhost", tcpServer.getPort());

        assertTrue(socket.isConnected());
    }

    @Test
    public void port_becomes_free_once_the_server_is_stopped() throws IOException {
        tcpServer.start();
        int port = tcpServer.getPort();
        tcpServer.stop();

        try (ServerSocket serverSocket = new ServerSocket(port)) {
            assertTrue(serverSocket.isBound());
        }
    }

    @Test
    public void does_not_attempt_to_open_the_port_until_started() throws IOException {
        ServerSocket serverSocket = new ServerSocket(0);
        int port = serverSocket.getLocalPort();
        TcpServer tcpServer = new TcpServer("localhost", port, connectionFactory);

        serverSocket.close();
        tcpServer.start();
        Socket socket = new Socket("localhost", port);

        assertTrue(socket.isConnected());
    }

    @Test
    public void connecting_to_the_port_makes_the_server_create_a_connection() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);

        tcpServer.start();
        Socket socket = new Socket("localhost", tcpServer.getPort());
        int localPort = socket.getLocalPort();
        tcpServer.applyUpdates();

        ArgumentCaptor<SocketChannel> arg = ArgumentCaptor.forClass(SocketChannel.class);
        verify(connectionFactory).create(arg.capture());
        int actualPort = ((InetSocketAddress) arg.getValue().getRemoteAddress()).getPort();
        assertEquals(localPort, actualPort);
    }

    @Test
    public void only_creates_a_single_channel_for_a_connection_when_apply_updates_is_called_multiple_times() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);
        tcpServer.start();
        new Socket("localhost", tcpServer.getPort());

        tcpServer.applyUpdates();
        tcpServer.applyUpdates();

        verify(connectionFactory, times(1)).create(any());
    }

    @Test
    public void writing_to_the_socket_makes_the_server_send_the_written_bytes_to_the_tcp_connection() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);

        tcpServer.start();
        Socket socket = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();
        socket.getOutputStream().write(new byte[] { 7 });
        tcpServer.applyUpdates();

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(tcpConnection).received(arg.capture(), eq(1));
        ByteBuffer buffer = arg.getValue();
        assertEquals(7, buffer.get());
    }

    @Test
    public void supports_receiving_a_512_byte_packet() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);

        tcpServer.start();
        Socket socket = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();
        socket.getOutputStream().write(new byte[512]);
        tcpServer.applyUpdates();

        verify(tcpConnection).received(any(), eq(512));
    }

    @Test
    public void supports_receiving_a_large_packet() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);
        doAnswer(i -> {
            ByteBuffer buffer = i.getArgumentAt(0, ByteBuffer.class);
            int count = i.getArgumentAt(1, int.class);
            buffer.get(new byte[count]);
            return null;
        }).when(tcpConnection).received(any(), anyInt());

        tcpServer.start();
        Socket socket = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();
        socket.getOutputStream().write(new byte[8192]);
        tcpServer.applyUpdates();

        verify(tcpConnection, times(16)).received(any(), eq(512));
    }

    @Test
    public void does_not_attempt_to_write_to_the_tcp_connection_on_disconnection() throws IOException {
        TcpConnection tcpConnection = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection);
        tcpServer.start();
        Socket socket = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();

        socket.close();
        tcpServer.applyUpdates();

        verify(tcpConnection, never()).received(any(), anyInt());
    }

    @Test
    public void recycles_the_same_buffer_across_multiple_clients() throws IOException {
        TcpConnection tcpConnection1 = mock(TcpConnection.class);
        TcpConnection tcpConnection2 = mock(TcpConnection.class);
        when(connectionFactory.create(any(SocketChannel.class))).thenReturn(tcpConnection1).thenReturn(tcpConnection2);
        tcpServer.start();
        Socket socket1 = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();
        Socket socket2 = new Socket("localhost", tcpServer.getPort());
        tcpServer.applyUpdates();

        socket1.getOutputStream().write(new byte[1]);
        socket2.getOutputStream().write(new byte[1]);
        tcpServer.applyUpdates();

        ArgumentCaptor<ByteBuffer> arg = ArgumentCaptor.forClass(ByteBuffer.class);
        verify(tcpConnection1).received(arg.capture(), eq(1));
        verify(tcpConnection2).received(arg.capture(), eq(1));
        List<ByteBuffer> allValues = arg.getAllValues();
        assertEquals(2, allValues.size());
        assertSame(allValues.get(0), allValues.get(1));
    }

    @AfterEach
    public void shutdownTcpServer() {
        tcpServer.stop();
    }
}
