package com.example.marketmaker;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ThroughputTest {
    class Clients {
        private final int connections;
        private final int microsPerRequest;
        private final String host;
        private final int port;
        private final ArrayList<Socket> sockets = new ArrayList<>();
        private final Random random = new Random(0);
        private int actualRequests;
        private volatile boolean stopRequested;

        public Clients(int connections, int microsPerRequest, String host, int port) {
            this.connections = connections;
            this.microsPerRequest = microsPerRequest;
            this.host = host;
            this.port = port;
        }

        public void connect(Runnable afterConnection) {
            for(int i = 0; i < connections; i++) {
                try {
                    sockets.add(new Socket(host, port));
                    afterConnection.run();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        public void startRequests() {
            byte[] message = "123 BUY 100\n".getBytes(StandardCharsets.US_ASCII);
            new Thread(() -> {
                long startTime = System.nanoTime();
                while(!stopRequested) {
                    long runningNs = System.nanoTime() - startTime;
                    int expectedRequests = (int)(runningNs / 1000 / microsPerRequest);
                    for(; actualRequests < expectedRequests; actualRequests++) {
                        Socket socket = sockets.get(random.nextInt(sockets.size()));
                        try {
                            socket.getOutputStream().write(message);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }).start();
        }

        public void stopRequests() {
            stopRequested = true;
        }
    }

    @Test
    public void supports_one_client_requesting_100000_quotes_per_second() throws InterruptedException {
        int microsPerRequest = 10;
        int connections = 1;
        int secondsToRun = 10;

        throughputTest(microsPerRequest, connections, secondsToRun);
    }

    @Test
    public void supports_10000_clients_requesting_10_quotes_per_second_each() throws InterruptedException {
        int microsPerRequest = 100;
        int connections = 10000;
        int secondsToRun = 10;

        throughputTest(microsPerRequest, connections, secondsToRun);
    }

    private void throughputTest(int microsPerRequest, int connections, int secondsToRun) throws InterruptedException {
        throughputTest(microsPerRequest, connections, secondsToRun, 1, (securityId, referencePrice, buy, quantity) -> 0);
    }

    @Test
    public void with_11_threads_supports_10_clients_requesting_100_quotes_per_second_each_if_producing_a_quote_takes_the_engine_10ms() throws InterruptedException {
        int microsPerRequest = 1000;
        int connections = 10;
        int secondsToRun = 10;

        throughputTest(microsPerRequest, connections, secondsToRun, 11, (securityId, referencePrice, buy, quantity) -> {
            try {
                Thread.sleep(10);
                return 0;
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void throughputTest(int microsPerRequest, int connections, int secondsToRun, int processingThreads, QuoteCalculationEngine calculationEngine) throws InterruptedException {
        AtomicInteger queued = new AtomicInteger();
        QuoteRequest poison = new QuoteRequest(null, 0, false, 0);
        QuoteRequestQueue queue = new QuoteRequestQueue() {
            @Override
            public void add(QuoteRequest request) {
                super.add(request);
                queued.incrementAndGet();
            }

            @Override
            public QuoteRequest takeOne() {
                queued.decrementAndGet();
                return super.takeOne();
            }
        };
        TcpServer tcpServer = new TcpServer("localhost", 0, new TcpConnectionFactory(queue));
        tcpServer.start();
        Clients clients = new Clients(connections, microsPerRequest, "localhost", tcpServer.getPort());
        clients.connect(tcpServer::applyUpdates);

        AtomicBoolean stop = new AtomicBoolean();
        new Thread(() -> {
            while(!stop.get()) {
                tcpServer.applyUpdates();
            }
        }).start();
        QuoteRequestProcessor processor = new QuoteRequestProcessor(calculationEngine, new ReferencePriceSource() {
            @Override
            public void subscribe(ReferencePriceSourceListener listener) {

            }

            @Override
            public double get(int securityId) {
                return 0;
            }
        });
        for(int i = 0; i < processingThreads; i++) {
            new Thread(() -> {
                while (!stop.get()) {
                    QuoteRequest quoteRequest = queue.takeOne();
                    if (quoteRequest == poison) {
                        return;
                    }
                    processor.process(quoteRequest);
                }
            }).start();
        }

        clients.startRequests();
        Thread.sleep(secondsToRun * 1000);
        clients.stopRequests();
        stop.set(true);
        for(int i = 0; i < processingThreads; i++) {
            queue.add(poison);
        }

        int expectedProcessed = secondsToRun * 1000000 / microsPerRequest;
        assertTrue(clients.actualRequests > expectedProcessed * 0.99, "Test framework was too slow to send requests");
        assertTrue(queued.get() < expectedProcessed * 0.01, "Queue contains " + queued.get() + " unprocessed items");
    }
}
