package com.example.marketmaker;

import java.nio.ByteBuffer;
import java.util.List;

public class TcpConnection {
    private final QuoteRequestExtractor extractor;
    private final QuoteRequestQueue queue;

    public TcpConnection(QuoteRequestExtractor extractor, QuoteRequestQueue queue) {
        this.extractor = extractor;
        this.queue = queue;
    }

    public void received(ByteBuffer buffer, int length) {
        List<QuoteRequest> requests = extractor.extractRequests(buffer, length);
        for (QuoteRequest request : requests) {
            request.register();
            queue.add(request);
        }
    }
}
