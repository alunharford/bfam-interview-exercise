package com.example.marketmaker;

import java.nio.channels.SocketChannel;

public class TcpConnectionFactory {
    private final QuoteRequestQueue queue;

    public TcpConnectionFactory(QuoteRequestQueue queue) {
        this.queue = queue;
    }

    public TcpConnection create(SocketChannel channel) {
        return new TcpConnection(new QuoteRequestExtractor(new QuoteChannel(channel)), queue);
    }
}
