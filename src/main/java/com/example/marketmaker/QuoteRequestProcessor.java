package com.example.marketmaker;

public class QuoteRequestProcessor {
    private final QuoteCalculationEngine calculationEngine;
    private final ReferencePriceSource referencePriceSource;

    public QuoteRequestProcessor(QuoteCalculationEngine calculationEngine, ReferencePriceSource referencePriceSource) {
        this.calculationEngine = calculationEngine;
        this.referencePriceSource = referencePriceSource;
    }

    public void process(QuoteRequest quoteRequest) {
        double referencePrice = referencePriceSource.get(quoteRequest.securityId);
        double quote = calculationEngine.calculateQuotePrice(quoteRequest.securityId, referencePrice, quoteRequest.isBuy, quoteRequest.quantity);

        quoteRequest.channel.sendResponse(quoteRequest, quote);
    }
}
