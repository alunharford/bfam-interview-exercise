package com.example.marketmaker;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class QuoteRequestExtractor {
    private final QuoteChannel channel;
    private ArrayList<QuoteRequest> result = new ArrayList<>();
    private int security;
    private boolean isBuy;
    private int quantity;
    private interface ProcessingMachineState {
        void process(byte b);
    }
    private class SecurityProcessingMachine implements ProcessingMachineState {
        @Override
        public void process(byte b) {
            if(b == ' ') {
                processingMachine = new BuySellProcessingMachine();
            } else {
                security = security * 10 + b - '0';
            }
        }
    }
    private class BuySellProcessingMachine implements ProcessingMachineState {
        private boolean hasDecidedResult;
        @Override
        public void process(byte b) {
            if(b == ' ') {
                processingMachine = new QuantityProcessingMachine();
            } else if(!hasDecidedResult) {
                isBuy = b == 'B';
                hasDecidedResult = true;
            }
        }
    }
    private class QuantityProcessingMachine implements ProcessingMachineState {
        @Override
        public void process(byte b) {
            if(b == '\n') {
                processingMachine = new SecurityProcessingMachine();
                result.add(new QuoteRequest(channel, security, isBuy, quantity));
                security = 0;
                quantity = 0;
            } else {
                quantity = quantity * 10 + b - '0';
            }
        }
    }
    private ProcessingMachineState processingMachine = new SecurityProcessingMachine();

    public QuoteRequestExtractor(QuoteChannel channel) {
        this.channel = channel;
    }

    public List<QuoteRequest> extractRequests(ByteBuffer buffer, int length) {
        result = new ArrayList<>();
        while(buffer.position() < length) {
            processingMachine.process(buffer.get());
        }
        return result;
    }
}
