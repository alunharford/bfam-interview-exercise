package com.example.marketmaker;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class QuoteChannel {
    private final SocketChannel channel;
    private final LinkedHashMap<QuoteRequest, ByteBuffer> queue = new LinkedHashMap<>();

    public QuoteChannel(SocketChannel channel) {
        this.channel = channel;
    }

    public void registerRequest(QuoteRequest request) {
        synchronized (queue) {
            queue.put(request, null);
        }
    }

    public void sendResponse(QuoteRequest request, double quote) {
        byte[] bytes = (Double.toString(quote) + '\n').getBytes(StandardCharsets.US_ASCII);
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        synchronized (queue) {
            queue.replace(request, buffer);
            try {
                Iterator<ByteBuffer> valueIterator = queue.values().iterator();
                while (valueIterator.hasNext()) {
                    ByteBuffer value = valueIterator.next();
                    if (value == null) {
                        return;
                    } else {
                        valueIterator.remove();
                        channel.write(value);
                    }
                }
            } catch (IOException e) {
                try {
                    channel.close();
                } catch (IOException innerException) {
                    throw new RuntimeException(innerException);
                }
            }
        }
    }
}
