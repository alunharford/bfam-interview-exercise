package com.example.marketmaker;

import java.util.concurrent.ArrayBlockingQueue;

public class QuoteRequestQueue {
    private final ArrayBlockingQueue<QuoteRequest> quoteRequests = new ArrayBlockingQueue<>(10000);

    public void add(QuoteRequest request) {
        try {
            quoteRequests.put(request);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public QuoteRequest takeOne() {
        try {
            return quoteRequests.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
