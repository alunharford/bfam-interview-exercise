package com.example.marketmaker;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class TcpServer {
    private final String hostname;
    private int port;
    private final TcpConnectionFactory connectionFactory;
    private ServerSocketChannel serverSocket;
    private Selector selector;
    private final ByteBuffer buffer = ByteBuffer.allocate(512);

    public TcpServer(String hostname, int port, TcpConnectionFactory connectionFactory) {
        this.hostname = hostname;
        this.port = port;
        this.connectionFactory = connectionFactory;
    }

    public void start() {
        try {
            selector = Selector.open();
            serverSocket = ServerSocketChannel.open();
            serverSocket.bind(new InetSocketAddress(hostname, port));
            serverSocket.configureBlocking(false);
            this.port = ((InetSocketAddress) serverSocket.getLocalAddress()).getPort();
            serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        try {
            if(serverSocket != null) {
                serverSocket.close();
                selector.selectNow();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getPort() {
        return port;
    }

    public void applyUpdates() {
        try {
            selector.select(key -> {
                if(key.isAcceptable()) {
                    newConnection(key);
                } else if(key.isReadable()) {
                    read(key);
                }
            }, 1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void newConnection(SelectionKey key) {
        try {
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel channel = server.accept();
            channel.configureBlocking(false);
            TcpConnection connection = connectionFactory.create(channel);
            channel.register(selector, SelectionKey.OP_READ, connection);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void read(SelectionKey key) {
        try {
            TcpConnection connection = (TcpConnection) key.attachment();
            SocketChannel channel = (SocketChannel) key.channel();
            int count;
            while ((count = channel.read(buffer)) > 0) {
                buffer.position(0);
                connection.received(buffer, count);
                buffer.position(0);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
