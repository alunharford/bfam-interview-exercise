package com.example.marketmaker;

public class QuoteRequest {
    public final QuoteChannel channel;
    public final int securityId;
    public final boolean isBuy;
    public final int quantity;

    public QuoteRequest(QuoteChannel channel, int securityId, boolean isBuy, int quantity) {
        this.channel = channel;
        this.securityId = securityId;
        this.isBuy = isBuy;
        this.quantity = quantity;
    }

    public void register() {
        channel.registerRequest(this);
    }
}
